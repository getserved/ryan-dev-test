const React = require('react');
const Button = require('./button');

function Wrapper() {
      
  return (
      <div>
        <div className='button-group'>
        <h2>default</h2>
        <Button/><Button disabled={true}/>
        </div>

        <div className='button-group'>
        <h2>primary</h2>
        <Button primary={true}/><Button primary={true} disabled={true}/>
        </div>

        <div className='button-group'>
        <h2>secondary</h2>
        <Button secondary={true}/><Button secondary={true} disabled={true}/>
        </div>

        <div className='button-group'>
        <h2>flat</h2>
        <Button flat={true}/><Button flat={true} disabled={true}/>
        </div>
      </div>
    ); 
}

module.exports = Wrapper;

