const React = require('react');


class  Button extends React.Component {

  constructor(props) {
    super(props);
    this.handleMouseDown = this.handleMouseDown.bind(this);
    this.handleMouseUp = this.handleMouseUp.bind(this);
    
  }
handleMouseDown(e) {
  if(!this.props.primary && !this.props.secondary && !this.props.flat) {
  e.target.style.backgroundColor = '#20C3FF';
  e.target.style.color = '#fff';
  }

  if(this.props.primary) {
  e.target.style.backgroundColor = 'rgb(255,120,0)';
  e.target.style.color = '#fff';
  }
}

handleMouseUp(e) {
  if(!this.props.primary && !this.props.secondary && !this.props.flat) {
  e.target.style.backgroundColor = '#fff';
  e.target.style.color = 'rgb(255,120,0)';
  }

  if(this.props.primary) {
  e.target.style.backgroundColor = '#fff';
  e.target.style.color = 'rgb(255,120,0)';
  }

}
      
  render() {return (
    <button className={'default ' + (this.props.primary ? 'primary' : 
                       this.props.secondary ? 'secondary' :
                       this.props.flat ? 'flat': '')} disabled={this.props.disabled} onMouseDown={this.handleMouseDown} onMouseUp={this.handleMouseUp}>
    {this.props.label || 'Button'}
    </button>
    ); 
}
}

module.exports = Button;
